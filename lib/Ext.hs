{-# LANGUAGE ForeignFunctionInterface #-}
module Ext
  ( c_crypt
  )
where

import Foreign.C.String

foreign import ccall "crypt.h crypt"
   c_crypt :: CString -> CString -> CString
