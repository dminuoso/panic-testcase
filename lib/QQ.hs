{-# LANGUAGE TemplateHaskell #-}
module QQ
  ( qq
  )
where

import Ext
import Foreign.Ptr
import Language.Haskell.TH.Quote
import Language.Haskell.TH.Syntax

qq :: QuasiQuoter
qq = QuasiQuoter
   { quoteExp = \_ -> do
        let x = c_crypt nullPtr nullPtr
        return (LitE (StringL (show x)))
   }
