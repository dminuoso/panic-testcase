{ pkgs ? import <nixpkgs> {}
, ghc ? "ghc8102"
}:

pkgs.stdenv.mkDerivation rec {
  name = "testcase";

  buildInputs = [
    pkgs.haskell.compiler.${ghc}
    pkgs.cabal-install
  ];
}
